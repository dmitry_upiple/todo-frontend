import React from 'react';
import './App.css';
import { FetchStages } from "./services/graphql/functions";
import Clear from './components/Clear';

function App() {

  return (
    <div className="App">
        <h1>My startup progress</h1>

        <div className="app-block">
            <FetchStages />
            <Clear/>
        </div>
    </div>
  );
}

export default App;
