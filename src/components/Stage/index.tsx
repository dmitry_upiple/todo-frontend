import React from "react";
import {COMPLETED_STAGE_IMAGE} from "../../common/constants";
import Item, {ItemInterface} from "../Item";
interface StageInterface {
    id: number;
    position: number;
    name: string;
    status: boolean; // false - completed, true - in progress
    todos: any[];
}

export default ({id, name, status, todos, position}: StageInterface) => {
    const rawStages = localStorage.getItem('stages');
    let stages;
    if (rawStages) {
        stages = JSON.parse(rawStages);
    }
    const currentStage = stages.find((item: any) => item.position === position );
    return (
        <div className="stage-block">
            <h2><span className="stage-header">{position}</span> {name} {!status && <img src={COMPLETED_STAGE_IMAGE} className="icon-yes" alt='yes'/>}</h2>
            {todos.map((todo: ItemInterface) => {
                return <Item editable={currentStage.editable} key={todo.id} id={todo.id} status={todo.status} name={todo.name}/>
            })}
        </div>
    )
}