import React from "react";
import {useMutation} from "@apollo/client";
import {GET_STAGE_LIST, UPDATE_TODO} from "../../services/graphql/query";
export interface ItemInterface {
    id: number;
    name: string;
    editable: boolean;
    status: boolean; // false - done, true - in progress
}

export default function ({id, name, status, editable}: ItemInterface) {

    const [updateTodo, { data, loading, error }] = useMutation(UPDATE_TODO, {
        refetchQueries: [
            GET_STAGE_LIST
        ],
    });
    return (
        <div>
            <input
                type='checkbox'
                readOnly={!editable}
                checked={!status}
                onChange={(e) => {
                    editable && updateTodo(
                        {
                            variables: {
                                id: id,
                                status: !e.target.checked,
                            },
                        }
                    ); return !e.target.value;
                }}
                id={`todo_${id}`}/>
            <label htmlFor={`todo_${id}`}>{name}</label>
        </div>
    )
}