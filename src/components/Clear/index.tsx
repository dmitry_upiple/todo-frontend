import React from "react";
import {useMutation} from "@apollo/client";
import {CLEAR_ALL_IN_DATABASE, GET_STAGE_LIST} from "../../services/graphql/query";

export default () => {
    const [clearAllInDatabase, { data, loading, error }] = useMutation(CLEAR_ALL_IN_DATABASE, {
        refetchQueries: [
            GET_STAGE_LIST
        ],
    });
    return (
        <>
            <button onClick={() => clearAllInDatabase()}>Clear All stages and todos in database</button>
        </>
    )
}
