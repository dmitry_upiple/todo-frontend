import {gql} from "@apollo/client";

const GET_STAGE_LIST = gql`
    query {
        stageList {
            status
            error
            response {
                id
                name
                status
                todos {
                    id
                    name
                    status
                }
            }
        }
    }
`;

const UPDATE_TODO = gql`
    mutation (
        $id: Int!
        $name: String
        $stageId: Int
        $sortOrder: Float
        $status: Boolean
    ) {
        todoEdit(
            payload: {
                id: $id
                name: $name
                stageId: $stageId
                sortOrder: $sortOrder
                status: $status
            }
        )
        {
            status
            error
            response {
                id
                name
                status
            }
        }
    }
    
`;
const CLEAR_ALL_IN_DATABASE = gql`
    mutation {
        clearAllInDatabase
    }
`;

export {
    GET_STAGE_LIST,
    UPDATE_TODO,
    CLEAR_ALL_IN_DATABASE,
};
