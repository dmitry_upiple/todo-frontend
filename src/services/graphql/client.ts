import {ApolloClient, InMemoryCache} from "@apollo/client";
import {GRAPHQL_URI} from "../../common/constants";

export const graphql_client = new ApolloClient({
    cache: new InMemoryCache(),
    uri: GRAPHQL_URI,
    ssrMode: true,
    defaultOptions: {
        watchQuery: {
            fetchPolicy: "no-cache",
        },
        query: {
            fetchPolicy: "no-cache",
        },
    },
});