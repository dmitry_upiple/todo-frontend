import React from "react";
import { useQuery } from "@apollo/client";
import { GET_STAGE_LIST } from "./query";
import Stage from "../../components/Stage";
interface EditableInterface {
    position: number;
    editable: boolean;
}
export function FetchStages() {
    const { loading, error, data } = useQuery(GET_STAGE_LIST);

    if (loading) return `Loading`;
    if (error) return error.message;

    if (data?.stageList?.status) {
        // Mapping editable phases
        let array: EditableInterface[] = [];

        data?.stageList?.response.forEach((stage: any, index: number) => {
            if (!stage.status) {
                array.push({
                    position: index+1,
                    editable: false,
                });
            } else {
                // Map all todo_ to make sure that we have at least one not completed
                const status = !!stage.todos.find((item: any) => item.status);
                if (index+1 === 1 && status) {
                    array.push({
                        position: index+1,
                        editable: true,
                    });
                } else if (index+1 === 1 && !status) {
                    array.push({
                        position: index+1,
                        editable: false,
                    });
                }
                if (index+1 !== 1) {
                    const check = !!array.find((item: any) => item.editable);
                    if (check) {
                        array.push({
                            position: index+1,
                            editable: false,
                        });
                    } else {
                        array.push({
                            position: index+1,
                            editable: true,
                        });
                    }
                }
            }
        });
        localStorage.setItem('stages', JSON.stringify(array));
        return data?.stageList?.response.map((stage: any, index: number) => {
            return <Stage key={stage.id} id={stage.id} position={index+1} name={stage.name} status={stage.status} todos={stage.todos}/>;
        });
    }
}